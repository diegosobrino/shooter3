﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player
{
    public string m_playerName = "Default";
    public float  m_points;

    public List<SimpleWeaponInfo> m_currentWeaponsOfPlayer;
    
}
