﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRocketLogic : MonoBehaviour
{
    public float speed;
    public float timeToExplode;
    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;  
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Collider>() != null)
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        timeToExplode += Time.deltaTime;

        if(timeToExplode >= 10)
        {
            Destroy(this.gameObject);
        }
    }
}
