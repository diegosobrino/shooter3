﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponScript : MonoBehaviour
{
    [Header("Public Variables")]
    public Transform m_raycastSpot;

    [Header("Accuracy Variables")]
    public float m_accuracyDropPerShot;
    public float m_accuracy;
    public float m_accuracyRecoverPerSecond;
    private float m_currentAccurracy;

    [Header("UI Variables")]
    public Texture2D m_crosshairTexture;
    public AudioClip m_fireSound;
    public TextMeshProUGUI Amunition;
    private bool m_canShot;

    [Header("Kind of Weapon")]
    public bool m_isShotgun;
    public bool m_isMachineGun;
    public bool m_isRocketLauncher;

    [Header("Specific Weapon Variables")]
    public float bulletsToShoot;
    float bulletsShot;

    float currentAmunition;
    public float m_simultaneousShots;
    public float m_recoilBack;
    float shootTimer;

    [Header("Ammo Prefabs")]
    public GameObject m_rocket;

    [SerializeField]
    private SimpleWeaponInfo data;

    private void Start()
    {
        currentAmunition = data.ammoCapacity;
        m_currentAccurracy = m_accuracy;
        GameManager.Instance.FindScoreText();
    }
    private void Update()
    {
        shootTimer += Time.deltaTime;
        m_currentAccurracy = Mathf.Lerp(m_currentAccurracy, m_accuracy, m_accuracyRecoverPerSecond * Time.deltaTime);

        if (m_isMachineGun)
        {
            m_canShot = true;
        }

        if (m_canShot)
        {
            if (shootTimer >= 1 / data.rateOfShot)
            {
                if (Input.GetButton("Fire1"))
                {
                    if (m_isRocketLauncher)
                    {
                        ShotRocket();
                    }
                    else
                    {
                        Shot();
                    }
                }
            }
        }
        else if (Input.GetButtonUp("Fire1") && !m_isMachineGun)
        {
            m_canShot = true;
        }

        Recharge();

        UpdateAmunition();
    }

    private void OnGUI()
    {
        Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
        Rect auxRect = new Rect(center.x - 20, center.y - 20, 20, 20);
        GUI.DrawTexture(auxRect, m_crosshairTexture, ScaleMode.StretchToFill);
    }

    private void ShotRocket()
    {
        shootTimer = 0;

        if (currentAmunition <= 0)
        {
            return;
        }

        currentAmunition--;

        m_canShot = false;

        for (int i = 0; i < m_simultaneousShots; i++)
        {
            GameObject proj = Instantiate(m_rocket, m_raycastSpot.position, m_raycastSpot.rotation) as GameObject;
        }

        this.transform.Translate(new Vector3(0, 0, -m_recoilBack), Space.Self);

        GetComponent<AudioSource>().PlayOneShot(m_fireSound);
    }

    private void Shot()
    {
        m_canShot = false;


        currentAmunition--;

        shootTimer = 0;

        if (m_isShotgun)
        {
            bulletsShot = bulletsToShoot;
        }
        else
        {
            bulletsShot = 1;
        }

        for (int i = 0; i < bulletsShot; i++)
        {
            float accuracyModifier = (100 - m_currentAccurracy) / 1000;
            Vector3 directionForward = m_raycastSpot.forward;
            directionForward.x += Random.Range(-accuracyModifier, accuracyModifier);
            directionForward.y += Random.Range(-accuracyModifier, accuracyModifier);
            directionForward.z += Random.Range(-accuracyModifier, accuracyModifier);
            m_currentAccurracy -= m_accuracyDropPerShot;
            m_currentAccurracy = Mathf.Clamp(m_currentAccurracy, 0, 100);

            Ray ray = new Ray(m_raycastSpot.position, directionForward);

            RaycastHit hit;

            //Aqui es donde hay que filtrar si es enemigo y llamar a el metodo de recivir daño
            if (Physics.Raycast(ray, out hit, data.weaponRange))
            {
                if (hit.transform.gameObject.GetComponent<IABase>())
                {
                    hit.transform.gameObject.GetComponent<IABase>().RecieveDamage((int)data.damage);
                }
                Debug.Log("Hit " + hit.transform.name);
                if (hit.rigidbody)
                {
                    hit.rigidbody.AddForce(ray.direction * data.forceToApply);
                    Debug.Log("Hit");
                }
            }
            Debug.DrawRay(m_raycastSpot.position, directionForward, Color.green, 4);
        }

        GetComponent<AudioSource>().PlayOneShot(m_fireSound);
    }

    private void Recharge()
    {
        if (Input.GetKeyDown(KeyCode.R) || currentAmunition <= 0)
        {
            if (currentAmunition <= data.ammoCapacity)
            {
                currentAmunition = data.ammoCapacity;
            }
            else
            {

            }

        }
    }

    public void UpdateAmunition()
    {
        Amunition.text = currentAmunition.ToString();
    }
}

