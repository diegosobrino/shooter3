using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Xml.Serialization;

public class MenuCanvasController : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject volumeMenu;
    public GameObject controlsMenu;
    public GameObject saveMenu;

    public Slider Sfx_Slider;
    public Slider Music_Slider;

    DataManager dataManager;

    private void Start()
    {
        MusicManager.Instance.PlayBackgroundMusic(AppSounds.MAIN_TITLE_MUSIC);
        dataManager = FindObjectOfType<DataManager>();
    }

    public void OnOnePlayer()
    {
        mainMenu.gameObject.SetActive(false);
        saveMenu.gameObject.SetActive(true);
    }

    public void OnOptions(bool options)
    {
        mainMenu.SetActive(!options);
        optionsMenu.SetActive(options);

        if (!options)
        {
            MusicManager.Instance.MusicVolumeSave = Music_Slider.value;
            MusicManager.Instance.SFXVolumeSave = Sfx_Slider.value;
        }
    }

    public void OnVolume(bool volume)
    {
        volumeMenu.SetActive(volume);
        optionsMenu.SetActive(!volume);
    }
    public void OnControls(bool volume)
    {
        controlsMenu.SetActive(volume);
        optionsMenu.SetActive(!volume);
    }

    public void OnExit()
    {
        Application.Quit();
    }

    public void OnSfxValueChanged()
    {
        MusicManager.Instance.SFXVolume = Sfx_Slider.value;
    }

    public void OnMusicValueChanged() 
    {
        MusicManager.Instance.MusicVolume = Music_Slider.value;
    }

    public void OnNewGame()
    {
        GameManager.Instance.newGame = true;
        Blackboard.m_gameType = GameType.PLAYER_VS_IA;
        MusicManager.Instance.PlayBackgroundMusic(AppSounds.BUTTON_SFX);
        SceneManager.LoadScene(AppScenes.LOADING_SCENE);
    }

    public void OnLoadGame()
    {
        if (File.Exists((Application.persistentDataPath + "/GameData.xml")))
        {
            GameManager.Instance.newGame = false;
            Blackboard.m_gameType = GameType.PLAYER_VS_IA;
            MusicManager.Instance.PlayBackgroundMusic(AppSounds.BUTTON_SFX);
            SceneManager.LoadScene(AppScenes.LOADING_SCENE);
        }
    }
}
