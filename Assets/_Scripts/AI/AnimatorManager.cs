using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimatorManager : MonoBehaviour
{
    Animator m_animator;
    IABase entity;
    NavMeshAgent navMeshAgent;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        entity = GetComponentInParent<IABase>();
        navMeshAgent = entity.GetComponent<NavMeshAgent>();
    }

    private void FixedUpdate()
    {
        if (navMeshAgent.velocity != Vector3.zero)
        {
            UpdateXZSpeed();
        }
    }


    public virtual void UpdateXZSpeed()
    {
        Vector3 localVelocity = transform.InverseTransformDirection(navMeshAgent.velocity);

        m_animator.SetFloat("XSpeed", localVelocity.x);
        m_animator.SetFloat("ZSpeed", localVelocity.z);
    }
}
