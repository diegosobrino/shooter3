using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionBase : MonoBehaviour
{
    public string actionAnimation;

    protected bool isPerformingAction;
    protected bool isInterrupted;
    

    private void Awake()
    {
        
    }

    protected virtual void OnEnterAction()
    {


    }

    protected virtual void OnPerformAction()
    {


    }
    protected virtual void OnExitAction()
    {


    }

    public virtual void OnInterrupt()
    {
        OnExitAction();

    }
}
