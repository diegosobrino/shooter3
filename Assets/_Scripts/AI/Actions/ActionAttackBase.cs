using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "AI/Actions/AttackAction")]

public class ActionAttackBase : ActionBase
{

    [Header("Action Configuration")]
    public int attackScore;
    public float recoveryTime;

    public float minDistance;
    public float maxDistance;

    public float minAngle;
    public float maxAngle;

}
