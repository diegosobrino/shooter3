using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    AIL ail;
    AreaSpawn areaParent;

    [Header("Spawn Configuration")]
    public int id;

    [Range(0, 25)]
    [SerializeField] float cadency;
    [SerializeField] List<GameObject> prefabList;
    float timer;

    private void Awake()
    {
        ail = GetComponentInParent<AIL>();
        areaParent = GetComponentInParent<AreaSpawn>();
    }
    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        if (timer>cadency)
        {
            timer = 0;
            SpawnUnit(0);
        }
    }
    void SpawnUnit(int i)
    {
        Instantiate(prefabList[i], transform.position, Quaternion.identity, areaParent.transform);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawCube(transform.position, Vector3.one);
    }

}
