using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSpawn : MonoBehaviour
{
    public List<Spawn> SpawnList;

    private void Awake()
    {
        int i = 0;
        foreach (Spawn sw in GetComponentsInChildren<Spawn>())
        {
            sw.id = i;
            SpawnList.Add(sw);
            i++;
        }
    }
}
