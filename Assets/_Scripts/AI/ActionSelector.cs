using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSelector : MonoBehaviour
{

    enum selectorType
    {
        Sequencial,
        Score,
    }

    [SerializeField] selectorType currentSelectorType;

    [SerializeField] List<ActionBase> ListActions;
    [SerializeField] List<ActionAttackBase> ListAttack;




    private void Awake()
    {
        foreach (ActionBase action in GetComponentsInChildren<ActionBase>())
        {
            ListActions.Add(action);
        }
        foreach (ActionAttackBase attackActioon in GetComponentsInChildren<ActionAttackBase>())
        {
            ListAttack.Add(attackActioon);
        }
    }



    public void SelectAction()
    {
        switch (currentSelectorType)
        {
            case selectorType.Sequencial:
                SequencialSelector();
                break;
            case selectorType.Score:
                ScoreSelector();
                break;
            default:
                break;
        }
        Debug.Log("Selecting action...");

    }


    void SequencialSelector()
    {
        //Read the sequence 
        //Autocontrol that actions are completed or interrupted
        //If sequencialAction1 is finished go to sequencialAction2
    }

    void ScoreSelector()
    {
        //Check Distance in AttacakList
        //Check Direction in AttackList

        //Add maxScore

        //Calculate randomScore

        //Select action
    }


    public void SelectAttack()
    {




        Debug.Log("Selecting attack...");

    }



 

   
}
