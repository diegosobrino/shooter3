using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
    protected IABase IABase;

    //States
    [HideInInspector] public StateIdle idleState;
    [HideInInspector] public StateChase chaseState;
    [HideInInspector] public StateCover coverState;
    [HideInInspector] public StateCombat combatState;
    [HideInInspector] public StatePatrol patrolState;

    [SerializeField] public StateBase currentState;
    [SerializeField] StateBase comeFromState;
    [SerializeField] float timeInState;
    [SerializeField] List<StateBase> listStates;

    enum states
    {
      idle, 
      patrol,
      follow,
      goToPosition,
    }
    [SerializeField] states inicialState;

    void Start()
    {
        IABase = GetComponent<IABase>();

        foreach (StateBase state in GetComponentsInChildren<StateBase>())
        {
            listStates.Add(state);
        }

        StartStateMachine();
    }


    void SetStates<T>(ref T initState)
    {
        foreach (StateBase state in listStates)
        {
            if (state.GetComponent<T>() == null)
            {
                return;
            }
            initState = state.GetComponent<T>();
        }
      
    }
    void StartStateMachine()
    {
        SetStates<StateIdle>(ref idleState);
        SetStates<StateChase>(ref chaseState);
        SetStates<StateCombat>(ref combatState);
        SetStates<StateCover>(ref coverState);
        SetStates<StatePatrol>(ref patrolState);

        if (currentState == null)
        {
            currentState = idleState;
          
        }

        currentState.OnEnter();
    }
  
    void Update()
    {
        if (currentState != null)
        {
            currentState.Execute();
        }
    }

    public void ChangeState(StateBase newState)
    {
        if (currentState != null)
        {
            comeFromState = currentState;
            currentState.OnExit();


            currentState = newState;

            currentState.OnEnter();

        }
    }
}
