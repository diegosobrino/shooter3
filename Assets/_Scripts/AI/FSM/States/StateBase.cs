using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateBase : MonoBehaviour
{
    protected string nameState;
    protected IABase entity;
    protected FSM fsm;

    protected virtual void Awake()
    {
        entity = GetComponentInParent<IABase>();
        fsm = GetComponentInParent<FSM>();
    }


    public virtual void OnEnter()
    {


    }

    public virtual void Execute()
    {


    }

    public virtual void OnExit()
    {



    }
}
