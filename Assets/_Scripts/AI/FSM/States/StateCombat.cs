using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateCombat : StateBase
{
    public override void OnEnter()
    {

        //Debug.Log("Combat");
        entity.animator.SetBool("Combat", true);
        entity.myNavMeshAgent.speed = entity.walkingCombatSpeed;
    }

    public override void Execute()
    {
        entity.GoToTarget();

        if (entity.CheckDistanceTarget() > entity.rangeCombat)
        {
            fsm.ChangeState(fsm.chaseState);
        }
    }
    public override void OnExit()
    {

        entity.animator.SetBool("Combat", false);
    }
}
