using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePatrol : StateBase
{

    float stoppingDistance = 1f;
    bool pausePatrol, reachedPoint, nextPointSelected, paused, startingPatrol;

    Area area;
    AreaPatrol areaPatrol;
    PatrolPoint currentPathPoint;
   

    public override void OnEnter()
    {
        entity.myNavMeshAgent.stoppingDistance = 0;

        if (!startingPatrol)
        {
            
            area = AIL.instance.GetNextArea(entity);
            areaPatrol = AIL.instance.GetNextPatrolAreaInZone(entity.areaIndex, entity);
            currentPathPoint = areaPatrol.pathPatrol[0];
            startingPatrol = true;

        }
        entity.myNavMeshAgent.SetDestination(currentPathPoint.transform.position);
        Debug.DrawLine(transform.position, currentPathPoint.transform.position, Color.green, 5f);
    }
    public override void Execute()
    {
        if (entity.myNavMeshAgent.remainingDistance <= stoppingDistance)
        {
            
            if (pausePatrol)
            {
                if (!reachedPoint)
                {
                    reachedPoint = true;
                    nextPointSelected = false;
                    StartCoroutine(PausePatrol());
                }

                if (!nextPointSelected && !paused)
                {
                    AIL.instance.GetNextPatrolPoint(entity.patrolIndex, entity.areaIndex,entity.areaPatrolIndex, entity);
                    StartCoroutine(PausePatrol());
                    nextPointSelected = true;
                }

            }
            else
            {
                if (!nextPointSelected)
                {
                    
           
                   
                    entity.myNavMeshAgent.SetDestination(AIL.instance.GetNextPatrolPoint(entity.patrolIndex, entity.areaIndex, entity.areaPatrolIndex, entity).transform.position);
                    StartCoroutine(PausePatrol());
                    nextPointSelected = true;
                }
            }
        }
        if (entity.CheckVisionAI())
        {
            fsm.ChangeState(fsm.chaseState);
        }
    }
    IEnumerator PausePatrol()
    {

        paused = true;
        yield return new WaitForSeconds(1f);
        paused = false;
        nextPointSelected = false;

    }



}
