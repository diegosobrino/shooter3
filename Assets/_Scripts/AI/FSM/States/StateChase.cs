using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateChase : StateBase
{
    public override void OnEnter()
    {
        //Debug.Log("Chase");
    
        entity.myNavMeshAgent.speed = entity.runSpeed;
        entity.myNavMeshAgent.stoppingDistance = 2;
    }
    public override void Execute()
    {

        entity.GoToTarget();


        if (entity.CheckDistanceTarget()<entity.rangeCombat)
        {
            fsm.ChangeState(fsm.combatState);
        }
    }





}
