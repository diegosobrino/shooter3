using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateIdle : StateBase
{
    protected enum Objetives
    {
        Nothing,
        KillPlayer,
        KillTarget,
        Listen,
        Patrol,
        Idle,
        FollowTarget,
        GoToTarget
    }

    [Header("Enitity")]
    [SerializeField] protected Objetives firstObjetive;
    [SerializeField] protected Objetives currentObjetive;

    public override void OnEnter()
    {
        SelectObjective();
    }
    public override void Execute()
    {
        base.Execute();
        if (entity.CheckVisionAI())
        {
            entity.animator.SetBool("Alert", true);
            fsm.ChangeState(fsm.chaseState);
        }
    }

    void SelectObjective()
    {
        switch (firstObjetive)
        {
            case Objetives.Nothing:
                currentObjetive = Objetives.Nothing;
                break;
            case Objetives.KillPlayer:
                currentObjetive = Objetives.KillPlayer;
       
                fsm.ChangeState(fsm.chaseState);
                break;
            case Objetives.KillTarget:
                fsm.ChangeState(fsm.chaseState);

                currentObjetive = Objetives.KillTarget;
                entity.target = entity.FindTarget(50f);
                fsm.ChangeState(fsm.chaseState);
                break;
            case Objetives.Listen:
                currentObjetive = Objetives.Listen;
                break;
            case Objetives.Patrol:
               
                currentObjetive = Objetives.Patrol;
                fsm.ChangeState(fsm.patrolState);

                break;
            case Objetives.Idle:
                currentObjetive = Objetives.Idle;
                break;
            case Objetives.FollowTarget:
                currentObjetive = Objetives.FollowTarget;
                break;
            case Objetives.GoToTarget:
                currentObjetive = Objetives.GoToTarget;
                break;
            default:
                break;
        }

    }
}
