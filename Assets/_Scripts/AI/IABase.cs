using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[DisallowMultipleComponent]
[RequireComponent(typeof(NavMeshAgent))]

[RequireComponent(typeof(FSM))]
[RequireComponent(typeof(ActionSelector))]
public class IABase : Character
{
    public int ID;
    public Character target;

    [Header("Character Stats")]
    public int maxHP = 320;
    public int currentHP;
    public int score;
    


    [Header("Components")]
    public FSM fsm;
    public NavMeshAgent myNavMeshAgent;
    public Animator animator;

    [Header("Movement Configuration")]
    public float walkingSpeed;
    public float runSpeed;
    public float walkingCombatSpeed;
    public float angularSpeed;

    [Header("Perception")]

    [Range(0, 160)]
    [SerializeField] float fieldOfview;
    [Range(0, 50)]
    [SerializeField] float rangeVision;

    //Developing
    [SerializeField] float rangeSound;
    [SerializeField] float soundSensity;

    float _angle;
    float _distance;

    [SerializeField] LayerMask obtacleLayerMask;
    [SerializeField] Collider[] charColliders;
    [SerializeField] List<Character> unitsInRange;
    Vector3 targetPos;

    [Header("Patrol System")]
    [Tooltip("blaa")]
    public int areaIndex;
    public int areaPatrolIndex;
    public int patrolIndex;

    [Header("Hostility")]
    [SerializeField] LayerMask hostilityLayerMask;
    [Header("Combat")]
    public float rangeCombat;

    protected virtual void Awake()
    {
        currentHP = maxHP;
        fsm = GetComponent<FSM>();
        myNavMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    public bool CheckVisionAI()
    {
        charColliders = Physics.OverlapSphere(transform.position, rangeVision, hostilityLayerMask);

        if (charColliders.Length == 0)
        {
            return false;
        }

        foreach (Collider charColl in charColliders)
        {
            if (DetectLayer.LayerInLayerMask(charColl.gameObject.layer, hostilityLayerMask))
            {
                unitsInRange.Add(charColl.GetComponent<Character>());
            }
        }

        foreach (Character agent in unitsInRange)
        {
            targetPos = agent.transform.position;
            _angle = Vector3.Angle(transform.forward, targetPos);
            //Obstacle
            if (Physics.Raycast(transform.position, CheckDirectionToPos(targetPos), CheckDistancetoPos(targetPos), obtacleLayerMask))
            {
                continue;
            }
            if (_angle/2 < fieldOfview)
            {
                target = agent;
                unitsInRange.Clear();
                return true;
            }
        }
        unitsInRange.Clear();
        return false;
    }


    public void GoToTarget()
    {
        myNavMeshAgent.SetDestination(target.transform.position);
    }

    public Vector3 CheckDirectionToPos(Vector3 pos)
    {
        return pos - transform.position;
    }
    public float CheckDistancetoPos(Vector3 pos)
    {
        return Vector3.Distance(transform.position, pos);
    }

    public Vector3 CheckDirectionTarget()
    {
        return target.transform.position - transform.position;
    }
    public float CheckDistanceTarget()
    {
        return Vector3.Distance(transform.position, target.transform.position);
    }

    public void FindPlayer()
    {

    }

    List<float> ListDistance = new List<float>();
    float lastDist;
    float dist;
    public Character FindTarget(float range)
    {
        charColliders = Physics.OverlapSphere(transform.position, range, hostilityLayerMask);

        foreach (Collider coll in charColliders)
        {
            if (charColliders.Length == 0)
            {
                Debug.LogWarning("No hay target");
                return null;
            }

            foreach (Collider charColl in charColliders)
            {
                if (DetectLayer.LayerInLayerMask(charColl.gameObject.layer, hostilityLayerMask))
                {
                    unitsInRange.Add(charColl.GetComponent<Character>());
                }
            }
        }

        int order = 0;
        lastDist=0;

        if (unitsInRange.Count == 0)
        {
            return null;
        }
        for (int i = 0; i < unitsInRange.Count; i++)
        {
            targetPos = unitsInRange[i].transform.position;
            dist = Vector3.Distance(transform.position, unitsInRange[i].transform.position);

            if (lastDist == 0)
            {
                lastDist = dist;
            }
            if (dist > lastDist)
            {
                order = i;
                lastDist = dist;

            }
        }
      
        target = unitsInRange[order];

        unitsInRange.Clear();
        ListDistance.Clear();

        return target;
    }

    public void OnDeath()
    {

        myNavMeshAgent.ResetPath();
        myNavMeshAgent.isStopped = true;
        myNavMeshAgent.enabled = false;

        GameManager.Instance.UpdateScore(score);

        GetComponent<Collider>().enabled = false;
        fsm.enabled = false;

    }
    public void RecieveDamage(int damage)
    {
        currentHP -= damage;
        if (currentHP <= 0)
        {
            Debug.Log("Muerto");
            OnDeath();
            animator.SetTrigger("Death");
        }
        else
        {
            Debug.Log("Hit");
            animator.SetTrigger("Hit");
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        
        float halfFOV = fieldOfview / 2.0f;
        Quaternion leftRayRotation = Quaternion.AngleAxis(-halfFOV, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(halfFOV, Vector3.up);
        Vector3 leftRayDirection = leftRayRotation * transform.forward;
        Vector3 rightRayDirection = rightRayRotation * transform.forward;
        Gizmos.DrawRay(transform.position, leftRayDirection * rangeVision);
        Gizmos.DrawRay(transform.position, rightRayDirection * rangeVision);

    }
}
