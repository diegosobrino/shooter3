using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaPatrol : MonoBehaviour
{
    public int ID;

   
    [Tooltip("Puedes acceder a estos puntos desde el AIL")]
    public List<PatrolPoint> pathPatrol;

    private void Awake()
    {
        SetPatrolPoints();
    }

    private void SetPatrolPoints()
    {
        pathPatrol.Clear();
        int indexer = 0;
        foreach (PatrolPoint patrolPoint in GetComponentsInChildren<PatrolPoint>())
        {

            patrolPoint.GetComponent<PatrolPoint>().index = indexer;
            indexer++;
            pathPatrol.Add(patrolPoint);

        }

    }
}
