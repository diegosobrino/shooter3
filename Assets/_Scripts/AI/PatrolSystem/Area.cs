using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Area Script")]
public class Area : MonoBehaviour
{
    public int ID;

    public List<AreaPatrol> ListAreaPatrol;
    public List<PatrolPoint> ListAllPatrolPoints;


    private void Awake()
    {
        foreach (AreaPatrol areapatrol in GetComponentsInChildren<AreaPatrol>())
        {
            ListAreaPatrol.Add(areapatrol);
        }
        foreach (PatrolPoint pp in GetComponentsInChildren<PatrolPoint>())
        {
            ListAllPatrolPoints.Add(pp);
        }
    }
}
