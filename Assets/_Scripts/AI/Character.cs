using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public enum Team
    {
        Ally,
        Enemy
    }

    [Tooltip("Equipo al que pertenece la unidad")]
    public Team UnitTeam;


}
