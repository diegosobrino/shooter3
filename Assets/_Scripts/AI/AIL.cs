using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIL : MonoBehaviour
{
    public static AIL instance;

    [Header ("Area System")]
    public List<Area> areaList;
    public List<IABase> enemyList;
    public List<AreaSpawn> areaSpawnList;
    public List<Spawn> spawnList;
    public List<AreaPatrol> areaPatrolList;

    [SerializeField] List<float> distanceToAreaList = new List<float>();
    int id = 0;
    int ID = 0;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        Initialization();
    }


    void Initialization()
    {
        foreach (Area area in GetComponentsInChildren<Area>())
        {
            area.ID = ID;
            areaList.Add(area);
            ID++;
        }
        for (int i = 0; i < areaList.Count; i++)
        {
            distanceToAreaList.Add(0);
        }

        //Add enemies to enemy list

        foreach (IABase entity in GetComponentsInChildren<IABase>())
        {
            id++;
            enemyList.Add(entity);
            entity.ID = id;
        }

        //Add area to list
        foreach (AreaSpawn areaSpawn in GetComponentsInChildren<AreaSpawn>())
        {
            areaSpawnList.Add(areaSpawn);
        }
       
        foreach (AreaPatrol areaPatrol in GetComponentsInChildren<AreaPatrol>())
        {
            areaPatrol.ID = ID;
            areaPatrolList.Add(areaPatrol);
            ID++;
        }

       

    }


   

    public Area GetNearestArea(Vector3 enemyPos, IABase entity)
    {
        int indexLessDistance = 0;
        float minDistance = 0;

        for (int i = 0; i < areaList.Count; i++)
        {
            distanceToAreaList[i] = Vector3.Distance(areaList[i].transform.position, enemyPos);

            if (minDistance == 0)
            {
                minDistance = distanceToAreaList[i];
            }
            if (minDistance >= distanceToAreaList[i])
            {
                minDistance = distanceToAreaList[i];
                indexLessDistance = i;
            }

            //Debug.Log("Step: " + i + "Min Distance is : " + minDistance + " and the area is + " + areaList[i]);

        }

        entity.areaIndex = indexLessDistance;

        return areaList[indexLessDistance];
    }

    bool inverse;
    public PatrolPoint GetNextPatrolPoint(int currentPatrolPoint, int indexArea, int indexPatrolArea, IABase entity)
    {
        //Debug.Log("Before: PP: " + currentPatrolPoint);
        PatrolPoint nextP;
        int maxPP = areaList[indexArea].ListAreaPatrol[indexPatrolArea].pathPatrol.Count;

        if (!inverse && currentPatrolPoint + 1 >= maxPP)
        {
            inverse = true;
        }
        if (inverse && currentPatrolPoint == 0)
        {
            inverse = false;
        }

        if (!inverse)
        {
            nextP = areaList[indexArea].ListAreaPatrol[indexPatrolArea].pathPatrol[currentPatrolPoint + 1];
            entity.patrolIndex = currentPatrolPoint + 1;

        }
        else
        {
            nextP = areaList[indexArea].ListAreaPatrol[indexPatrolArea].pathPatrol[currentPatrolPoint + -1];
            entity.patrolIndex = currentPatrolPoint - 1;
        }
        //Debug.Log("After: PP: " + entity.patrolIndex);
        return nextP;

    }

    int indexOfNearest = 0;
    int k = 0;
    float lastDist = 0;
    public Area GetNextArea(IABase entity)
    {
        Area nextArea = null;
        lastDist = 0;
        indexOfNearest = 0;
        k = 0;

        foreach (Area area in areaList)
        {
            float dist = Vector3.Distance(entity.transform.position, area.transform.position);

            if (lastDist == 0)
            {
                lastDist = dist;
            }
            if (dist < lastDist)
            {
                lastDist = dist;
                indexOfNearest = k;
            }
            k++;
        }
        entity.areaIndex = indexOfNearest;
        nextArea = areaList[indexOfNearest];
        return nextArea;
    }

    //REFACTORIZAR Y FUNCIONA!. 
    public AreaPatrol GetNextPatrolAreaInZone(int indexArea, IABase entity)
    {
        AreaPatrol newAreaPatrol = null;
        lastDist = 0;
        indexOfNearest = 0;
        k = 0;


        foreach (AreaPatrol areaPatrol in areaList[indexArea].ListAreaPatrol)
        {
            float dist = Vector3.Distance(entity.transform.position, areaPatrol.transform.position);

          
            if (lastDist == 0)
            {
                lastDist = dist;
            }
            if (dist<lastDist)
            {
                lastDist = dist;
                indexOfNearest = k;
            }
            k++;
        }

        entity.areaPatrolIndex = indexOfNearest;
        newAreaPatrol = areaPatrolList[indexOfNearest];
        return newAreaPatrol;
    }
   
}
