using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    private Player m_player;


    void Start()
    {
        GenerateNewPlayer();

        if (GameManager.Instance.newGame)
        {
            Debug.Log("Has creado una partida nueva");
            SaveXMLPlayer();
            SaveGameScene();
            Debug.Log("El nombre del jugador se ha guardado con exito: " + m_player.m_playerName);
            GameManager.Instance.newGame = false;
            return;
        }

        if (File.Exists((Application.persistentDataPath + "/GameData.xml")))
        {
            LoadGameScene();
            LoadXmlPlayer();
            Debug.Log("El nombre del jugador se ha cargado con exito: " + m_player.m_playerName);
            Debug.Log("La puntuacion del jugador se ha cargado con exito: " + m_player.m_points);
        }
        else
        {
            Debug.Log("Has creado una partida nueva porque no hab�a nada guardado en disco");
            SaveXMLPlayer();
            SaveGameScene();
            Debug.Log("El nombre del jugador se ha guardado con exito: " + m_player.m_playerName);
        }

    }
    public void SaveXMLPlayer()
    {
        m_player.m_points = GameManager.Instance._score;
        XmlSerializer serializer = new XmlSerializer(typeof(Player));
        FileStream file = File.Create(Application.persistentDataPath + "/GameData.xml");
        serializer.Serialize(file, m_player);
        Debug.Log("Ubicacion del archivo: " + Application.persistentDataPath);
        file.Close();
    }
    public void LoadXmlPlayer()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Player));
        FileStream file = File.Open(Application.persistentDataPath + "/GameData.xml", FileMode.Open);
        m_player = (Player)serializer.Deserialize(file);

        GameManager.Instance._score = (int)m_player.m_points;
        GameManager.Instance.UpdateScore(0);

        Debug.Log(m_player.m_playerName);
    }

    public void GenerateNewPlayer()
    {
        // Creamos datos del jugador
        m_player = new Player();
        m_player.m_playerName = "Paquito el Ranchero";
        m_player.m_points = 0;
       
    }



    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            SaveGameScene();
            Debug.Log("Has guardado la escena con exito");
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            LoadGameScene();
            Debug.Log("Has cargado la escena con exito");
        }
    }
    public void SaveGameScene()
    {
        SaveableMonoBehaviour[] saveGOVector = FindObjectsOfType<SaveableMonoBehaviour>();
        List<TransformData> tranformList = new List<TransformData>();

        for (int i = 0; i < saveGOVector.Length; i++)
        {
            tranformList.Add(saveGOVector[i].GetData());
        }

        XmlSerializer serializer = new XmlSerializer(typeof(List<TransformData>));
        FileStream file = File.Create(Application.persistentDataPath + "/GameSceneData.xml");
        serializer.Serialize(file, tranformList);
        file.Close();
    }

    public void LoadGameScene()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(List<TransformData>));
        FileStream file = File.Open(Application.persistentDataPath + "/GameSceneData.xml", FileMode.Open);
        List<TransformData> transformList = (List<TransformData>)serializer.Deserialize(file);

        SaveableMonoBehaviour[] saveGOVector = FindObjectsOfType<SaveableMonoBehaviour>();

        for (int i = 0; i < saveGOVector.Length; i++)
        {
            for (int j = 0; j < transformList.Count; j++)
            {
                if (saveGOVector[i].transform.name.Equals(transformList[j].m_name))
                {
                    saveGOVector[i].SetData(transformList[j]);
                    transformList.Remove(transformList[j]);
                    break;
                }
            }
        }
    }

    public void SaveAll()
    {
        SaveXMLPlayer();
        SaveGameScene();
    }

    public void LoadAll()
    {
        LoadXmlPlayer();
        LoadGameScene();
    }
}
