using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon Info", menuName = "Weapon Info", order = 51)]
public class SimpleWeaponInfo : ScriptableObject
{
    [SerializeField]
    private string       m_name;
    new public string name
    {
        get
        {
            return m_name;
        }
    }

    [SerializeField]
    private float        m_damage                = 80f;
    public float damage
    {
        get
        {
            return m_damage;
        }
    }

    [SerializeField]
    private float        m_forceToApply          = 20f;
    public float forceToApply
    {
        get
        {
            return m_forceToApply;
        }
    }

    [SerializeField]
    private float        m_weaponRange           = 999f;
    public float weaponRange
    {
        get
        {
            return m_weaponRange;
        }
    }

    [SerializeField]
    private float        m_rateOfShot            = 3;
    public float rateOfShot
    {
        get
        {
            return m_rateOfShot;
        }
    }

    [SerializeField]
    private float        m_ammoCapacity          = 10;
    public float ammoCapacity
    {
        get
        {
            return m_ammoCapacity;
        }
    }

    [SerializeField]
    private AudioClip    m_fireSound;
    [SerializeField]
    private AudioClip    m_reloadSound;

}
