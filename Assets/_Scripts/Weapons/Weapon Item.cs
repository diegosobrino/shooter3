using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponItem 
{
    [Header("Weapon Attributes")]
    public string               m_name;
    [Tooltip("The damage of the weapon")]
    public float                m_damage = 80f;
    [Tooltip("The force of impact this weapon deals onto other objects")]
    public float                m_forceToApply = 20f;
    [Tooltip("The range of the weapon")]
    public float                m_weaponRange = 999f;
    [Tooltip("The bullets per second it can shoot")]
    public float                m_rateOfShot = 3;
    [Tooltip("The ammo it has")]
    public float                m_ammoCapacity = 10;
    [Tooltip("The amount of accuracy that will be lost between shots")]
    public float                m_accuracyDropPerShot;
    [Tooltip("The accuracy of the weapon")]
    public float                m_accuracy;
    public float                m_accuracyRecoverPerSecond;
    [Tooltip("How many bullets the weapon can shoot each shot")]
    public float                m_simultaneousShots;
    public float                m_recoilBack;
    [Space]
    [Space]

    [Header("What kind of shooting style this weapon has")]
    public bool                 m_isShotgun;
    public bool                 m_isMachineGun;
    public bool                 m_isRocketLauncher;
    [Space]
    [Space]

    [Header("Aesthetic settings")]
    public Texture2D            m_crosshairTexture;
    [Header("Sounds")]
    public AudioClip            m_fireSound;
    public AudioClip            m_reloadSound;
    [Tooltip("Kind of projectile the rocket will launch")]
    public GameObject           m_rocket;
}
