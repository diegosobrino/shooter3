using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WeaponEditor : EditorWindow
{
    WeaponList weaponInventoryList;

    int viewIndex;

    [MenuItem("Window/WeaponEditor")]
   static void Init()
    {
        EditorWindow.GetWindow(typeof(WeaponEditor));
    }

    void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objetcPath = "Assets/Resources/Data/WeaponList.asset";
            weaponInventoryList = AssetDatabase.LoadAssetAtPath(objetcPath, typeof(WeaponList)) as WeaponList;
        }

        if(weaponInventoryList == null)
        {
            viewIndex = 1;

            WeaponList asset = ScriptableObject.CreateInstance<WeaponList>();
            AssetDatabase.CreateAsset(asset, "Assets/WeaponList.asset");
            AssetDatabase.SaveAssets();

            weaponInventoryList = asset;

            if (weaponInventoryList)
            {
                weaponInventoryList.weaponList = new List<WeaponItem>();
                string relPath = AssetDatabase.GetAssetPath(weaponInventoryList);
                EditorPrefs.SetString("Object Path", relPath);
            }
        }
    }

    private void OnGUI()
    {
        GUILayout.Label("Weapon Editor", EditorStyles.boldLabel);
        GUILayout.Space(10);

        if(weaponInventoryList != null)
        {
            PrintTopMenu();
        }
        else
        {
            GUILayout.Space(10);
            GUILayout.Label("Cant load Weapon List");
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(weaponInventoryList);
        }
    }

    void PrintTopMenu()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        if(GUILayout.Button("<- Prev", GUILayout.ExpandWidth(false)))
        {
            if (viewIndex > 1)
                viewIndex--;
        }
        GUILayout.Space(5);
        if (GUILayout.Button("Next ->", GUILayout.ExpandWidth(false)))
        {
            if (viewIndex < weaponInventoryList.weaponList.Count)
            {
                viewIndex++;
            }
        }
        GUILayout.Space(60);
        if(GUILayout.Button("+ Add Weapon", GUILayout.ExpandWidth(false)))
        {
            AddItem();
        }
        GUILayout.Space(5);
        if(GUILayout.Button("- Delete Weapon", GUILayout.ExpandWidth(false)))
        {
            DeleteItem(viewIndex - 1);
        }
        GUILayout.EndHorizontal();

        if(weaponInventoryList.weaponList.Count > 0)
        {
            WeaponListMenu();
        }
        else
        {
            GUILayout.Space(10);
            GUILayout.Label("This weapon list is empty");
        }
    }

    void AddItem()
    {
        WeaponItem newItem = new WeaponItem();
        newItem.m_name = "New weapon";
        weaponInventoryList.weaponList.Add(newItem);
        viewIndex = weaponInventoryList.weaponList.Count;
    }

    void DeleteItem(int index)
    {
        weaponInventoryList.weaponList.RemoveAt(index);
    }

    void WeaponListMenu()
    {
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Weapon", viewIndex, GUILayout.ExpandWidth(false)), 1, weaponInventoryList.weaponList.Count);
        EditorGUILayout.LabelField("of   " + weaponInventoryList.weaponList.Count.ToString() + " weapon", "", GUILayout.ExpandWidth(false));
        GUILayout.EndHorizontal();

        string[] _choices = new string[weaponInventoryList.weaponList.Count];
        for (int i = 0; i < weaponInventoryList.weaponList.Count; i++)
        {
            _choices[i] = weaponInventoryList.weaponList[i].m_name;
        }
        int _choiceIndex = viewIndex - 1;
        viewIndex = EditorGUILayout.Popup(_choiceIndex, _choices) + 1;

        GUILayout.Space(10);
        weaponInventoryList.weaponList[viewIndex - 1].m_name = EditorGUILayout.TextField("Name", weaponInventoryList.weaponList[viewIndex - 1].m_name as string);
        weaponInventoryList.weaponList[viewIndex - 1].m_damage = EditorGUILayout.FloatField("Damage", weaponInventoryList.weaponList[viewIndex - 1].m_damage);

        GUILayout.Space(10);

        GUILayout.Label("MachineGun");

        weaponInventoryList.weaponList[viewIndex - 1].m_isMachineGun = (bool)EditorGUILayout.Toggle("Yes / No", weaponInventoryList.weaponList[viewIndex - 1].m_isMachineGun, GUILayout.ExpandWidth(false));

        GUILayout.Label("RocketLauncher");

        weaponInventoryList.weaponList[viewIndex - 1].m_isRocketLauncher = (bool)EditorGUILayout.Toggle("Yes / No", weaponInventoryList.weaponList[viewIndex - 1].m_isRocketLauncher, GUILayout.ExpandWidth(false));

        GUILayout.Label("Shotgun");

        weaponInventoryList.weaponList[viewIndex - 1].m_isShotgun = (bool)EditorGUILayout.Toggle("Yes / No", weaponInventoryList.weaponList[viewIndex - 1].m_isShotgun, GUILayout.ExpandWidth(false));
    }
}
