﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card 
{
    public string  m_cardName;
    public float   m_cost;
    public string  m_description;
    public int     m_type;
    //public Sprite  m_sprite;
    //public Color   m_color;
}