﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item 
{
    public string  m_itemName;
    public float   m_power;
    public string  m_description;
    //public Sprite  m_sprite;
}