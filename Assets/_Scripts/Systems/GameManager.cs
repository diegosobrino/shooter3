using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : PersistentSingleton<GameManager>
{
    private static GameManager _instance;
    [HideInInspector]
    public bool newGame;

    [HideInInspector]
    private TextMeshProUGUI scoreText;

    public int _score = 0;

    protected override void Awake()
    {
        base.Awake();

        _instance = this;       
        _score = 0;

    }

    public void FindScoreText()
    {
        TextMeshProUGUI[] textsInScene = FindObjectsOfType<TextMeshProUGUI>();

        foreach (TextMeshProUGUI n in textsInScene)
        {
            if (n == null)
                return;
            if (n.tag == "ScoreText")
            {
                scoreText = n;
            }
        }

        scoreText.text = _score.ToString();
    }

    public void UpdateScore(int points)
    {
        _score += points;
        scoreText.text = _score.ToString();
    }
   
}
