using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppScenes 
{
    public static readonly string MAIN_SCENE = "MainMenu";
    public static readonly string LOADING_SCENE = "LoadingScene"; 
    public static readonly string GAME_SCENE = "Level1";  
}

public class AppPlayerPrefKeys
{
    public static readonly string MUSIC_VOLUME = "MusicVolume";
    public static readonly string SFX_VOLUME = "SFXVolume"; 
}

public class AppPaths
{
    public static readonly string PERSISTENT_DATA = Application.persistentDataPath;
    public static readonly string PATH_RESOURCE_SFX = "Music/MenuSfx/";
    public static readonly string PATH_RESOURCE_MUSIC = "Music/MusicMenu/";
}

public class AppSounds
{
    public static readonly string MAIN_TITLE_MUSIC = "MainTitle";
    public static readonly string GAME_MUSIC = "Gameplay";
    public static readonly string BUTTON_SFX = "Click_Soft_01";
}
