using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    AsyncOperation m_operation;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += FinishLoading;
        StartCoroutine(ProgressBar());  
    }

    IEnumerator ProgressBar()
    {
        m_operation = SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
        m_operation.allowSceneActivation = false;

        while (!m_operation.isDone)
        {
            Debug.Log(m_operation.progress);
            yield return null;
        }
    }

    void FinishLoading(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= FinishLoading;
        m_operation.allowSceneActivation = true;
    }
}
