using UnityEngine;
using System.Collections;


public class DetectLayer
{
	public static bool LayerInLayerMask(int layer, LayerMask layerMask)
	{
		return (((1 << layer) & layerMask) != 0);
	}
}
